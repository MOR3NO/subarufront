
//**************************************************** Setup de eventos a escuchar
require('events').EventEmitter.defaultMaxListeners = 20;
//************************************************************* Codigo para crear servidor
// Express inicia servidor 
const express=require('express');
const app=express();

app.use(express.static(__dirname + '/public'));//Carpeta de donde sirve / carpeta raiz public

const server=app.listen(8888, () => {
  	console.log('Servidor web iniciado');
	});

//************************************************************** Coneccion de socketio a servidor en servicio
//Socket configuration 
var io = require('socket.io')(server); //Bind socket.io to our express server.


//*************************************************************** Configuracion para puerto serial

// Open & Read serial port data

io.on('connection',(socket) => {socket.on('serialportstart', ()=>{

		//----Inicio

		var SerialPort = require('serialport');
		const Readline = require('@serialport/parser-readline'); //Analizador 
		const port = new SerialPort('COM3', { baudRate: 9600 }); //Puerto
		const parser = port.pipe(new Readline({ delimiter: '\n' }));// Caracter objetivo
		
        port.on("open", () => { console.log('Serial port open');});

		parser.on('data', data =>{
  		  
  		const buffer = Buffer.alloc(10, data);
  		console.log(buffer.toString());

 		 io.emit('word', {data});
  
		});

		io.on('connection',(socket) => {socket.on('eject', ()=> {console.log("palabra");port.write('A');});});

		io.on('connection',(socket) => {socket.on('ejectoff', ()=>{port.write('a')});});

		io.on('connection',(socket) => {socket.on('radio', ()=>{port.write('B')});});

		io.on('connection',(socket) => {socket.on('radiooff', ()=>{port.write('b')});});

		io.on('connection',(socket) => {socket.on('back', ()=>{port.write('G')});});

		io.on('connection',(socket) => {socket.on('backoff', ()=>{port.write('g')});});

		io.on('connection',(socket) => {socket.on('home', ()=>{port.write('H')});});

		io.on('connection',(socket) => {socket.on('homeoff', ()=>{port.write('h')});});

		io.on('connection',(socket) => {socket.on('map', ()=>{port.write('C')});});

		io.on('connection',(socket) => {socket.on('mapoff', ()=>{port.write('c')});});

		io.on('connection',(socket) => {socket.on('media', ()=>{port.write('D')});});

		io.on('connection',(socket) => {socket.on('mediaoff', ()=>{port.write('d')});});

		io.on('connection',(socket) => {socket.on('apps', ()=>{port.write('E')});});

		io.on('connection',(socket) => {socket.on('appsoff', ()=>{port.write('e')});});

		io.on('connection',(socket) => {socket.on('forward', ()=>{port.write('F')});});

		io.on('connection',(socket) => {socket.on('forwardoff', ()=>{port.write('f')});});

		io.on('connection',(socket) => {socket.on('vol', ()=>{port.write('I')});});

		io.on('connection',(socket) => {socket.on('voloff', ()=>{port.write('i')});});

		io.on('connection',(socket) => {socket.on('tune', ()=>{port.write('K')});});

		io.on('connection',(socket) => {socket.on('tuneoff', ()=>{port.write('k')});});

		io.on('connection',(socket) => {socket.on('welcome', ()=>{port.write('J')});});

		io.on('connection',(socket) => {socket.on('nfc', ()=>{port.write('n')});});

		//--- Fin

		//--Cierra el puerto serial 	
		io.on('connection',(socket) => {socket.on('serialportclose', ()=>{
			
		port.close(function(err){if (err){return console.log("Serial port already closed");};});});});

		});});

//*****************************  42Q ***********************************

var querystring = require('querystring');
var http = require('http');


// Home manda llamar a esta conexion
io.on('connection', (socket) => {		
	           
        socket.on('pass', function(serialnumber) {pass(serialnumber); });//console.log("Contactado pass");
		socket.on('fail', function(serialnumber) {console.log("Contactado fail");fail(serialnumber); });
		socket.on('workstation', function(serialnumber){console.log("Contactado workstation");workstation(serialnumber); });
				
	});	

function pass(serialnumber) {
	
// Build the post string from an object
let sn = serialnumber;
console.log(sn);
     var post_data = {
         "projectname" : "harman",
         "unit":"945",
         "employee_number": "TST99999",
         "password" : "",
         "serial" : ""+sn+"",
		 "failcode" : "FA09",              
		 "position" : "Posicion lab"
	};

	var  myJSON = JSON.stringify(post_data);
    console.log(" Asi es como manda la peticion al servidor: "+myJSON+"\n");
	
     // An object of options to indicate where to post to
     var post_options = {
         host: '143.116.205.46',
         port: '8080',
         path: '/Aquiles_Back/conduit/pass',
         method: 'POST',
		 headers: {'Content-type':'application/json'}
     };
	 
	// Set up the request object
     var post_req = http.request(post_options, function(res) {
         res.on('data', function (chunk) {
         console.log('Response: ' + chunk);
         });
     });

     // post the data
     post_req.write(myJSON);
     post_req.end();

}


function fail(serialnumber) {
    // Build the post string from an object
let sn = serialnumber;  
console.log(sn);
  var post_data = {
         "projectname" : "harman",
         "unit":"945",
         "employee_number": "TST99999",
         "password" : "",
         "serial" : ""+sn+"",		 
		 "failcode" : "T103",
		 "position" : "FVT FUNCTIONALITY FAILURE"
	};

	var  myJSON = JSON.stringify(post_data);
    console.log(" Asi es como manda la peticion al servidor: "+myJSON+"\n");
	
     // An object of options to indicate where to post to
     var post_options = {
         host: '143.116.205.46',
         port: '8080',
         path: '/Aquiles_Back/conduit/fail',
         method: 'POST',
		 headers: {'Content-type':'application/json'}
     };
	 
	// Set up the request object
     var post_req = http.request(post_options, function(res) {
         res.on('data', function (chunk) {
         console.log('Response: ' + chunk);
         });
     });

     // post the data
     post_req.write(myJSON);
     post_req.end();

}


function workstation(serialnumber) {
	
let sn = serialnumber;
console.log(sn);
// Build the post string from an object
	let res;
     var post_data = {
         "projectname" : "harman",
         "unit":"945",
         "employee_number": "TST99999",
         "password" : "",
         
	};

	var  myJSON = JSON.stringify(post_data);
    //console.log(" Asi es como manda la peticion al servidor: "+myJSON+"\n");
	
     // An object of options to indicate where to post to 
     var post_options = {
         host: '143.116.205.46',
         port: '8080',

		 //--------------------------------------Sfdcpc---/numero de serie de la unidad
         path: '/Aquiles_Back/infoSerial/summary/p2448dc5/'+sn+'',
         method: 'GET',
		 headers: {'Content-type':'application/json'}
     };
	 
	let data='';
	let json;
	// Set up the request object
     var post_req = http.request(post_options, function(chunk) {		
			// Collect all chunks in data
			chunk.on('data', function (response) {			
			data += response;
			});			
		});		
	 setTimeout(function espera(){io.emit('workstationresponse',data)},1000);
     // post the data
     post_req.write(myJSON);
     post_req.end();

}




	