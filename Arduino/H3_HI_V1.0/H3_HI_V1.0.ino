//************************************************Leds
#include <Wire.h>
#define ADDRESS                        0b1100000
#define REGISTERS                      0b10000000
#define LED_OUTPUT_GROUP               0b11
int incomingByte=0; // for incoming serial data
//*********************************************Puerto Serial
#define NUMBER_OF_SHIFT_CHIPS   1
#define DATA_WIDTH   NUMBER_OF_SHIFT_CHIPS * 10
#define ADDRESS                        0b1100000
#define REGISTERS                      0b10000000
#define LED_OUTPUT_GROUP               0b11
 
int LoadPin    = 8;
int EnablePin  = 9;
int DataPin    = 11;
int ClockPin   = 12;

unsigned long pinValues;
unsigned long oldPinValues;

 int pinA = 2;  // Connected to CLK on (1)
 int pinB = 3;  // Connected to DT on  (1)
 int pinC = 4;  // Connected to CLK on (2)
 int pinD = 5;  // Connected to DT on  (2)
 int encoder1PosCount = 0;
 int encoder2PosCount = 0; 
 int pinALast;
 int pinCLast;  
 int aValA;
 int aValC;
 boolean bCW;
 boolean dCW;
 


//********************************************
void setup() {
 
//********************Setup de ICLEDS****************
Wire.begin();
  Serial.begin(9600);
  // Transmit to the TLC59116
  Wire.beginTransmission(ADDRESS);
  // Send the control register.  All registers will be written to, starting at register 0
  Wire.write(byte(REGISTERS));
  // Set MODE1: no sub-addressing
  Wire.write(byte(0));
  // Set MODE2: dimming
  Wire.write(byte(0));
  // Set individual brightness control to maximum+-
  for (int i=0; i< 16; i++)
    Wire.write(byte(0xff));  //0XFF 
  // Set GRPPWM: Full brightness
  Wire.write(byte(0xff));  //0XFF
  // Set GRPFREQ: Not blinking, must be 0
  Wire.write(byte(0));
  // Set LEDs off for now
  for (int i=0; i< 4; i++)
  Wire.write(byte(0x00));
  // Set the I2C all-call and sub addresses (if needed)//
  Wire.endTransmission();

//****************************** Setup temporal de puerto para prueba
  Serial.begin(9600); // opens serial port, sets data rate to 9600 bps

//***************************** Setup Serial Port
   pinMode (pinA,INPUT);
   pinMode (pinB,INPUT);
   pinMode (pinC,INPUT);
   pinMode (pinD,INPUT);
   pinALast = digitalRead(pinA);
   pinALast = digitalRead(pinC);   
   
         
   pinMode(LoadPin, OUTPUT);
   pinMode(EnablePin, OUTPUT);
   pinMode(ClockPin, OUTPUT);
   pinMode(DataPin, INPUT);

   digitalWrite(ClockPin, LOW);
   digitalWrite(LoadPin, HIGH);

   pinValues = read_shift_regs();
   print_byte();
   oldPinValues = pinValues;

//******************************************Serial port end  
}//Cierra setup

void loop() {
  // send data only when you receive data:
 if (Serial.available() > 0) {
    // read the incoming byte:
    incomingByte = Serial.read();


     switch (incomingByte){

      case 65://Ascci A---------EJECT-----------//

              {
               Wire.write(byte(REGISTERS + 0x14 )); 
               Wire.write(byte(0b00000001));
               delay(250);
               Wire.endTransmission(); 
               break;
              
             }
             
      case 97://Ascii a---------EJECTOFF-------//

            {
               Wire.write(byte(REGISTERS + 0x14 )); 
               Wire.write(byte(0b00000000));
               delay(250);
               Wire.endTransmission(); 
               break;
             } 
       case 66://Ascci B--------RADIO----------//

              {
               Wire.write(byte(REGISTERS + 0x14 )); 
               Wire.write(byte(0b00000100));
               delay(250);
               Wire.endTransmission(); 
               break;
              
             }
             
      case 98://Ascii b----------RADIOOFF------//

            {
               Wire.write(byte(REGISTERS + 0x14 )); 
               Wire.write(byte(0b00000000));
               delay(250);
               Wire.endTransmission(); 
               break;
             }
      case 67://Ascci C-----------MAP---------//

              {
               Wire.write(byte(REGISTERS + 0x14 )); 
               Wire.write(byte(0b00010000));
               delay(250);
               Wire.endTransmission(); 
               break;
              
             }
             
      case 99://Ascii c----------MAPOFF-------//

            {
               Wire.write(byte(REGISTERS + 0x14 )); 
               Wire.write(byte(0b00000000));
               delay(250);
               Wire.endTransmission(); 
               break;
             }
             
      case 68://Ascci D---------MEDIA---------//

              {
               Wire.write(byte(REGISTERS + 0x14 )); 
               Wire.write(byte(0b01000000));
               delay(250);
               Wire.endTransmission(); 
               break;
             }
             
      case 100://Ascii d--------MEDIAOFF------//

            {
               Wire.write(byte(REGISTERS + 0x14 )); 
               Wire.write(byte(0b00000000));
               delay(250);
               Wire.endTransmission(); 
               break;
             }
      case 69://Ascci E----------APPS--------//

              {
               Wire.write(byte(REGISTERS + 0x15 )); 
               Wire.write(byte(0b00000001));
               delay(250);
               Wire.endTransmission(); 
               break;           
             }
             
      case 101://Ascii e-------APPSOFF-------//

            {
               Wire.write(byte(REGISTERS + 0x15 )); 
               Wire.write(byte(0b00000000));
               delay(250);
               Wire.endTransmission(); 
               break;
             }
             
      case 70://Ascci F--------FORWARD-------//

              {
               Wire.write(byte(REGISTERS + 0x15 )); 
               Wire.write(byte(0b00000100));
               delay(250);
               Wire.endTransmission(); 
               break;
              
             }
             
      case 102://Ascii f-----FORWARDOFF----//

            {
               Wire.write(byte(REGISTERS + 0x15 )); 
               Wire.write(byte(0b00000000));
               delay(250);
               Wire.endTransmission(); 
               break;
             }
      case 71://Ascci G---------BACK-------// 

              {
               Wire.write(byte(REGISTERS + 0x15 )); 
               Wire.write(byte(0b00010000));
               delay(250);
               Wire.endTransmission(); 
               break;
             }
             
      case 103://Ascii g-------BACKOFF-----//

            {
               Wire.write(byte(REGISTERS + 0x15 )); 
               Wire.write(byte(0b00000000));
               delay(250);
               Wire.endTransmission(); 
               break;
             }
             
      case 72://Ascci H--------HOME-------//

              {
               Wire.write(byte(REGISTERS + 0x15 )); 
               Wire.write(byte(0b01000000));
               delay(250);
               Wire.endTransmission(); 
               break;
              
             }
             
      case 104://Ascii h------HOMEOFF-----//

            {
               Wire.write(byte(REGISTERS + 0x15 )); 
               Wire.write(byte(0b00000000));
               delay(250);
               Wire.endTransmission(); 
               break;
             }
             
      case 73://Ascci I------LFKNUP------//

              {
               Wire.write(byte(REGISTERS + 0x16 )); 
               Wire.write(byte(0b00000101));
               delay(250);
               Wire.endTransmission(); 
               break;
             }
             
      case 105://Ascii i---LFKNUPOFF-----//

            {
               Wire.write(byte(REGISTERS + 0x16 )); 
               Wire.write(byte(0b00000000));
               delay(250);
               Wire.endTransmission(); 
               break;
             }
             
      case 74://Ascci J-----Intro----//

              { 
                
              Serial.println("Case J :-)");  
              
               //----Led 0
               Wire.write(byte(REGISTERS + 0x14 )); 
               Wire.write(byte(0b00000001));
               delay(50);
               Wire.endTransmission(); 
               
               Wire.write(byte(REGISTERS + 0x14 )); 
               Wire.write(byte(0b00000000));
               delay(50);
               Wire.endTransmission(); 

               Wire.write(byte(REGISTERS + 0x16 )); 
               Wire.write(byte(0b00000101));
               delay(50);
               Wire.endTransmission();

               Wire.write(byte(REGISTERS + 0x16 )); 
               Wire.write(byte(0b00000000));
               delay(50);
               Wire.endTransmission(); 

               Wire.write(byte(REGISTERS + 0x15 )); 
               Wire.write(byte(0b00010000));
               delay(50);
               Wire.endTransmission();

               Wire.write(byte(REGISTERS + 0x15 )); 
               Wire.write(byte(0b00000000));
               delay(50);
               Wire.endTransmission();
               
               Wire.write(byte(REGISTERS + 0x14 )); 
               Wire.write(byte(0b00000100));
               delay(50);
               Wire.endTransmission();
               
               Wire.write(byte(REGISTERS + 0x14 )); 
               Wire.write(byte(0b00000000));
               delay(50);
               Wire.endTransmission();

               Wire.write(byte(REGISTERS + 0x15 )); 
               Wire.write(byte(0b01000000));
               delay(50);
               Wire.endTransmission();
               
               Wire.write(byte(REGISTERS + 0x15 )); 
               Wire.write(byte(0b00000000));
               delay(50);
               Wire.endTransmission();

               Wire.write(byte(REGISTERS + 0x14 )); 
               Wire.write(byte(0b00010000));
               delay(50);
               Wire.endTransmission(); 

               Wire.write(byte(REGISTERS + 0x14 )); 
               Wire.write(byte(0b00000000));
               delay(50);
               Wire.endTransmission();

               Wire.write(byte(REGISTERS + 0x14 )); 
               Wire.write(byte(0b01000000));
               delay(50);
               Wire.endTransmission(); 

               Wire.write(byte(REGISTERS + 0x14 )); 
               Wire.write(byte(0b00000000));
               delay(50);
               Wire.endTransmission();
               Wire.write(byte(REGISTERS + 0x15 )); 
               Wire.write(byte(0b00000100));
               delay(50);
               Wire.endTransmission();

               Wire.write(byte(REGISTERS + 0x15 )); 
               Wire.write(byte(0b00000000));
               delay(50);
               Wire.endTransmission();
               
               Wire.write(byte(REGISTERS + 0x15 )); 
               Wire.write(byte(0b00000001));
               delay(50);
               Wire.endTransmission();
               
               Wire.write(byte(REGISTERS + 0x15 )); 
               Wire.write(byte(0b00000000));
               delay(50);
               Wire.endTransmission(); 

               Wire.write(byte(REGISTERS + 0x16 )); 
               Wire.write(byte(0b01010000));
               delay(50);
               Wire.endTransmission();

               Wire.write(byte(REGISTERS + 0x16 )); 
               Wire.write(byte(0b00000000));
               delay(50);
               Wire.endTransmission();

               
              
              /*
              Wire.write(byte(REGISTERS + 0x14)); 
              Wire.write(byte(0b1111111));
              Wire.write(byte(REGISTERS + 0x15)); 
              Wire.write(byte(0b1111111));
              Wire.write(byte(REGISTERS + 0x16)); 
              Wire.write(byte(0b1111111));
              Wire.write(byte(REGISTERS + 0x17)); 
              Wire.write(byte(0b1011111));
              delay(250);

              Wire.endTransmission();
                             
              Wire.write(byte(REGISTERS + 0x14 )); 
              Wire.write(byte(0b00000000));
              delay(50);
              Wire.endTransmission();
              Wire.write(byte(REGISTERS + 0x15 )); 
              Wire.write(byte(0b00000000));
              delay(50);
              Wire.endTransmission();
              Wire.write(byte(REGISTERS + 0x16 )); 
              Wire.write(byte(0b00000000));
              delay(50);
              Wire.endTransmission();
              Wire.write(byte(REGISTERS + 0x17 )); 
              Wire.write(byte(0b00000000));
              delay(50);
              Wire.endTransmission(); 

               */
               Wire.write(byte(REGISTERS + 0x17 )); 
               Wire.write(byte(0b00000001));
               delay(150);
               Wire.endTransmission();
               
               Wire.write(byte(REGISTERS + 0x17 )); 
               Wire.write(byte(0b00000000));
               delay(150);
               Wire.endTransmission(); 

               Wire.write(byte(REGISTERS + 0x17 )); 
               Wire.write(byte(0b00000001));
               delay(150);
               Wire.endTransmission();
               
               Wire.write(byte(REGISTERS + 0x17 )); 
               Wire.write(byte(0b00000000));
               delay(150);
               Wire.endTransmission();
              /*
              Wire.write(byte(REGISTERS + 0x15 )); 
              Wire.write(byte(0b00000000));
              delay(50);
              
              Wire.endTransmission();
              Wire.write(byte(REGISTERS + 0x16 )); 
              Wire.write(byte(0b00000000));
              delay(50);
              
              Wire.endTransmission();
              Wire.write(byte(REGISTERS + 0x14 )); 
              Wire.write(byte(0b00000000));
              delay(50);

              Wire.write(byte(REGISTERS + 0x17 )); 
              Wire.write(byte(0b00000000));
              delay(50);
              Wire.endTransmission(); 


           //   Wire.endTransmission();
              delay(1000);
              Wire.endTransmission();
              */
              break;
             }
             
      case 106://Ascii j---LFKNDWNOFF---//

            {
               Wire.write(byte(REGISTERS + 0x16 )); 
               Wire.write(byte(0b00000000));
               delay(250);
               Wire.endTransmission(); 
               break;
             }
             
      case 75://Ascci K-----RGKNDUP-----//

              {
               Wire.write(byte(REGISTERS + 0x16 )); 
               Wire.write(byte(0b01010000));
               delay(250);
               Wire.endTransmission(); 
               break;
             }
             
      case 107://Ascii k---RGKNDUPOFF---//

            {
               Wire.write(byte(REGISTERS + 0x16 )); 
               Wire.write(byte(0b00000000));
               delay(250);
               Wire.endTransmission(); 
               break;
             }
      /*case 76://Ascci L---RGKNDOWN----//

              {
               Wire.write(byte(REGISTERS + 0x16 )); 
               Wire.write(byte(0b01000000));
               delay(250);
               Wire.endTransmission(); 
               break;              
             }
             
      case 108://Ascii l--RGKNDOWNOFF--//

            {
               Wire.write(byte(REGISTERS + 0x16 )); 
               Wire.write(byte(0b00000000));
               delay(250);
               Wire.endTransmission(); 
               break;
             }
             */
      case 77://Ascci M

              {
               Wire.write(byte(REGISTERS + 0x17 )); 
               Wire.write(byte(0b00000001));
               delay(250);
               Wire.endTransmission(); 
               break;
              
             }
             
      case 109://Ascii m

            {
               Wire.write(byte(REGISTERS + 0x17 )); 
               Wire.write(byte(0b00000000));
               delay(250);
               Wire.endTransmission(); 
               break;
             } 

             //----------------Funiones start
             

             //-----------Fin de funcinoes de start
     }// Cierra switch
         
 }//Cierra if principal

     //***Funciones de leds

 //*******************************************************Loop Serial Port 

 //Right knob
aValA = digitalRead(pinA);
   if (aValA != pinALast){ // Means the knob is rotating
     // if the knob is rotating, we need to determine direction
     // We do that by reading pin B.
     if (digitalRead(pinB) != aValA) {  // Means pin A Changed first - We're Rotating Clockwise
       encoder1PosCount --;
       bCW = true;
     } else {// Otherwise B changed first and we're moving CCW
       bCW = false;
       encoder1PosCount++;
     }
     //Serial.print("rk"); //"RK" Serial.print("rightknob")
     if (bCW){
       Serial.println("RKL");
     }else{
       Serial.println("RKR");
     }
     //Serial.println(encoder1PosCount);
   } 
   pinALast = aValA;
   ////////////////////////////////////////////////////////////////////////////////

  //Left Knob
   aValC = digitalRead(pinC);
   if (aValC != pinCLast){ // Means the knob is rotating
     // if the knob is rotating, we need to determine direction
     // We do that by reading pin B.
     if (digitalRead(pinD) != aValC) {  // Means pin A Changed first - We're Rotating Clockwise
       encoder2PosCount ++;
       dCW = true;
     } else {// Otherwise B changed first and we're moving CCW
       dCW = false;
       encoder2PosCount--;
     }
    
     if (dCW){
       Serial.println("LKR");//Serial.println(encoder2PosCount);
     }else{
       Serial.println("LKL");//Serial.println(encoder2PosCount);
     }
      
   } 
   pinCLast = aValC;

///////////////////////////////////////////////////////////////////// Botones Byteword
pinValues = read_shift_regs();

    if(pinValues != oldPinValues)
    {
        print_byte();
        oldPinValues = pinValues;
    }
}
  unsigned long read_shift_regs()
{
    long bitVal;
    unsigned long bytesVal = 0;

    digitalWrite(EnablePin, HIGH);
    digitalWrite(LoadPin, LOW);
    delayMicroseconds(5);
    digitalWrite(LoadPin, HIGH);
    digitalWrite(EnablePin, LOW);

    for(int i = 0; i < DATA_WIDTH; i++)
    {
        bitVal = digitalRead(DataPin);
        bytesVal |= (bitVal << ((DATA_WIDTH-1) - i));

        digitalWrite(ClockPin, HIGH);
        delayMicroseconds(5);
        digitalWrite(ClockPin, LOW);
    }
    return(bytesVal);
}

void print_byte() { 
  byte i; 

  for(byte i=0; i<=DATA_WIDTH-1; i++) 
  { 
    Serial.print(pinValues >> i & 1, BIN); 
    
  }   
  Serial.print("\n"); 
 //******************************************* Start leds intro 

  

 

//********************************************* Loop Serial port End
}//Cierre de loop
